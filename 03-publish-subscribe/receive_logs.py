#!/usr/bin/env python
import pika

conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

chan = conn.channel()

chan.exchange_declare(exchange='logs', type='fanout')
result = chan.queue_declare(exclusive=True)
queue_name = result.method.queue

chan.queue_bind(exchange='logs', queue=queue_name)

print(' [*] Waiting for messages.  To exit press CTRL+C')

def callback(ch, method, properties, body):
    print(" [x] %r" % (body,))

chan.basic_consume(callback, queue=queue_name, no_ack=True)

chan.start_consuming()
