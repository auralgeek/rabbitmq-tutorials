#!/usr/bin/env python
import pika
import sys

conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

chan = conn.channel()
chan.exchange_declare(exchange='logs', type='fanout')

msg = ' '.join(sys.argv[1:]) or 'Hello World!'
chan.basic_publish(exchange='logs', routing_key='', body=msg)

print(" [x] Sent %r" % msg)
conn.close()
