#!/usr/bin/env python
import pika
import sys

conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

chan = conn.channel()
chan.exchange_declare(exchange='direct_logs', type='direct')

severity = sys.argv[1] if len(sys.argv) > 1 else 'info'
msg = ' '.join(sys.argv[1:]) or 'Hello World!'
chan.basic_publish(exchange='direct_logs', routing_key=severity, body=msg)

print(" [x] Sent %r: %r" % (severity, msg))
conn.close()
