#!/usr/bin/env python
import pika
import sys

conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

chan = conn.channel()

chan.queue_declare(queue='task_queue', durable=True)

msg = ' '.join(sys.argv[1:]) or 'Hello World!'
chan.basic_publish(exchange='', routing_key='task_queue', body=msg, properties=pika.BasicProperties(delivery_mode=2,))

print(" [x] Sent %r" % msg)

conn.close()
