#!/usr/bin/env python
import pika
import sys

conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
chan = conn.channel()

chan.exchange_declare(exchange='topic_logs', type='topic')

routing_key = sys.argv[1] if len(sys.argv) > 1 else 'anonymous.info'
msg = ' '.join(sys.argv[2:]) or 'Hello World!'
chan.basic_publish(exchange='topic_logs', routing_key=routing_key, body=msg)

print(" [x] Sent %r: %r" % (routing_key, msg))
conn.close()
