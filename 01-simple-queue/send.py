#!/usr/bin/env python
import pika

conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

chan = conn.channel()

chan.queue_declare(queue='hello')

msg = 'Hello World!'
chan.basic_publish(exchange='', routing_key='hello', body=msg)

print(" [x] Sent %s" % msg)

conn.close()
